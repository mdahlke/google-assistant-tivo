<?php
define('VOICE_DEBUG', true);

require_once('lib/Util.php');
require_once('lib/Voice_Controller_Request.php');
require_once('lib/Voice_Controller_Response.php');
require_once('lib/Voice_Model_Request.php');
require_once('lib/Voice_Model_Response.php');

/**
 * Thanks buddel
 * @link http://php.net/manual/en/function.array-search.php#91365
 *
 * @param $needle
 * @param $haystack
 * @return bool|int|string
 */
function recursive_array_search($needle, $haystack) {
    foreach ($haystack as $key => $value) {
        $current_key = $key;
        if ($needle === $value OR (is_array($value) && recursive_array_search($needle, $value) !== false)) {
            return $current_key;
        }
    }
    return false;
}

function requestLog($message = '') {
    shell_exec('echo "' . $message . '" >> request.log');
}